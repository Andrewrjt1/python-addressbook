#!/usr/bin/python3
import csv
import os
fileloc="info.csv"#location info for all the csv reading portions
clear = lambda:os.system("clear")#clear for linux
currentselect = 0#holds selction id for all the functions
currentrelation = []#hold selection relations to interact with various functions
sortedinfo = []#list that holds the display infomation to be sorted if needed
csvreader ={}#dictonary for the csv reader if required was using a older way at first and didnt have time to change
def csv_read():#function that starts the csvreader used in other functions when required
	global csvreader
	csvfile = open(fileloc)#changelocation
	csvreader = csv.DictReader(csvfile)
def add_contact():#add contact function called from main menu loops over the csv to start with to see if there is a empty place from deletion if there is makes that the id and inserts into it there
	clear()
	global csvreader
	csv_read()
	newid = 0
	indexno = 1
	for k in csvreader:
		compare = int(k["id"])
		if indexno != compare:
			newid=indexno
			break
		indexno +=1
	if newid == 0:
		newid = indexno
	newperson = []
	newperson.append(newid)
	newname = input("What is the persons name?")
	newperson.append (newname)
	newadd = input("What is the persons address?")
	newperson.append(newadd)
	newmob = input("What is the persons mobile?")
	newperson.append(newmob)
	newemail = input("What is the persons email?")
	newperson.append(newemail)
	newcomp = input("What is the persons comapany?")
	newperson.append(newcomp)
	newcompno = input("What is the persons company number?")
	newperson.append(newcompno)
	newperson.append(add_relation(0))
	with open(fileloc, "r") as readFile:
		reader = csv.reader(readFile)
		lines = list(reader)
		placer = 0
		complete = True
		for full in lines:
			if not full:
				break
				complete = False
			else:
				placer+=1
		print(placer)
		print(lines)
		if complete == True:
			lines.append([""])
		lines[placer] = newperson
		with open(fileloc, "w",newline='') as writeFile:
			writer = csv.writer(writeFile)
			writer.writerows(lines)
	readFile.close()
	writeFile.close()
def add_relation(i):#creates the relation passes in variable for 2 different states one is more making contact which has a different format than editing
	global csvreader
	pid = ""
	name =""
	relation =""
	hopelist = []
	complete = ""
	view_contacts()
	csv_read()
	who = int(input("Who would you like to add type the number next to thier name"))
	for a in csvreader:
		viewcheck = int(a["id"])
		if who == viewcheck:
			pid = a["id"]
			name = a["name"]
			relation = input("what relation are they to the contact")
			complete+=pid+"|"+name+"|"+relation
	if i==1:
		cleanlist = complete.split("_")
		for clean in cleanlist:
			newclean = clean.split("|")
			hopelist.append(newclean)
		return hopelist
	else:
		return complete
def edit_person():#edits the contact finds the index point for where the edit needs to be inserted then does it
	clear()
	global currentselect
	with open(fileloc, "r") as readFile:#changelocation
		reader = csv.reader(readFile)
		lines = list(reader)
		indexpoint = 0
		for total in lines:
			try:
				checker = int(total[0])
			except ValueError:
				checker = 0
			if checker==currentselect:
				break
			indexpoint +=1
		editlist = list(lines[indexpoint])
		print("1.Name",editlist[1])
		print("2.Address",editlist[2])
		print("3.Mobile",editlist[3])
		print("4.Email",editlist[4])
		print("5.Company",editlist[5])
		print("6.Company number",editlist[6])
		print("7.Relations")
		display_relations()#have funtion to display/edit
		looper = True
		while looper==True:
			editno = int(input("what would you like to change? or type 8 to exit"))
			if editno == 8:
				lines[indexpoint]=editlist
				with open(fileloc, "w",newline='') as writeFile: #changelocation
					writer = csv.writer(writeFile)
					writer.writerows(lines)
				readFile.close()
				writeFile.close()
				display_details()
				break
			if editno == 7:
				newrelation = edit_relations()
				editlist[7]=newrelation
				continue
			if editno !=7:
				editcontent = input("would would you like"+editlist[editno]+"to change to?")
				editlist[editno]=editcontent
			print("change successful type 8 to exit or keep editing")
def edit_relations():#function used to edit the relations area first in converts the infomation into a 2d list the does what it needs to then reconvets and pushed the infomation back into the edit function
	global csvreader
	global currentselect
	global currentrelation
	cleanlist = []
	test = currentrelation.split("_")
	for clean in test:
		newclean = clean.split("|")
		cleanlist.append(newclean)
	loped=True
	while loped==True:
		for show in cleanlist:
			print(show[0],show[1],show[2])
		relinput = int(input("which one would you like to edit? press 0 to add new ones 99 to exit"))
		for show2 in cleanlist:
			if int(show2[0]) == relinput:
				changer = int(input("1 for name and 2 for relation"))
				if changer ==1:
					show2[1]=input("what would you like the name to be?")
				else:
					show2[2]=input("what would you like the relation to change as")
			if relinput == 0:
				new = add_relation(1)
				cleanlist.append([])
				amount = len(cleanlist)-1
				for ik in new:
					cleanlist[amount].append(ik[0])
					cleanlist[amount].append(ik[1])
					cleanlist[amount].append(ik[2])
				break
			if relinput == 99:
				firstlist = []
				for remake in cleanlist:
					remake1 = "|".join(remake)
					firstlist.append(remake1)
				freshlist = "_".join(firstlist)
				loped = False
				return freshlist
def delete_contact():#delete contact what it really does is make a empty line where it is so that it can be used later for add new user
	global currentselect
	row = []
	with open(fileloc, "r") as readFile:
		reader = csv.reader(readFile)
		lines = list(reader)
		lines[currentselect] = row
		with open(fileloc, "w",newline='') as writeFile:
			writer = csv.writer(writeFile)
			writer.writerows(lines)
	readFile.close()
	writeFile.close()
def view_contacts():#this shows the list of all the people in the address book it also adds that infomation to the list used for sorted if needed
	clear()
	global csvreader
	global sortedinfo
	csv_read()
	counter = 0
	sortedinfo.clear()
	for i in csvreader:
		print(i["id"],i["name"],i["company"])
		sortedinfo.append([])
		sortedinfo[counter].append(i["id"])
		sortedinfo[counter].append(i["name"])
		sortedinfo[counter].append(i["company"])
		counter+=1
def sort_option():#this pulls in the list for sorting and sorts accordingly
	global sortedinfo
	ask = int(input("would you like to sort the data? 0 for no otherwise press 1 to sort by name or 2 to sort by company"))
	if ask == 0:
		return
	elif ask == 1:
		clear()
		sortedinfo = sorted(sortedinfo, key=lambda x: x[1])
		for i in sortedinfo:
			print(i[0],i[1],i[2])
	elif ask ==2:
		clear()
		sortedinfo = sorted(sortedinfo, key=lambda x: x[2])
		for x in sortedinfo:
			print(x[0],x[1],x[2])
def view_more(inus):#this begins the view more details if checks if the person exists and sets the number to what they have selected 
	global csvreader
	global currentselect
	csv_read()
	check_exist = 0
	for x in csvreader:
		infocheck = int(x["id"])
		if inus == infocheck:
			check_exist = 1
			currentselect = infocheck
			return currentselect
		elif inus == 0:
			return
	if check_exist == 0:
		input("no selection of that type press any key to return to menu")
		main_menu()
def display_details():# takes the gloabal varible assigned in the previous function and then generates the display
	clear()
	global csvreader
	global currentselect
	csv_read()
	for a in csvreader:
		viewcheck = int(a["id"])
		if currentselect == viewcheck:
			print ("Name",a["name"])
			print ("Address",a["address"])
			print ("Mobile",a["mobile"])
			print ("Company",a["company"])
			print ("Company Number",a["cno"])
			print ("Relations to others you know")
			display_relations()
			print("1.Edit details")
			print("2.Delete contact")
			print("3.Back to menu")
			vselect = int(input("what would you like to do?"))
			if vselect == 1:
				edit_person()
			elif vselect == 2:
				confirm = int(input("are you sure you want to delete? press 8 to delete"))
				if confirm == 8:
					delete_contact()
				else:
					display_details()
			elif vselect == 3:
				return
			else:
				input("wrong selection")
				display_details()
def display_relations():#seprate function to display the relations converts the content and puts it into a list for use if required later
	global csvreader
	global currentselect
	global currentrelation
	csv_read()
	cleanlist = []
	for inc in csvreader:
		viewcheck = int(inc["id"])
		if currentselect == viewcheck:
			currentrelation = inc["relations"]
			test = inc["relations"].split("_")
			for clean in test:
				newclean = clean.split("|")
				cleanlist.append(newclean)
	for display in cleanlist:
		print("They know",display[1])
		print("and they know them because",display[2])
def print_menu():#menu builder
	print (30 * "-","MENU",30*"-")
	print ("1.View Contacts")
	print ("2.Add New Contact")
	print ("3.Exit")
def main_menu():#actual menu functon
	global currentselect
	loop=True
	while loop:
		clear()
		print_menu()
		select = int(input("What would you like to do?"))
		if select==1:
			view_contacts()
			sort_option()
			viewselect=int(input("Which one would you like to view futher input the number otherwise press 0 to return to menu"))
			view_more(viewselect)
			display_details()
		elif select==2:
			add_contact()
		elif select==3:
			loop=False
		else:
			print("wrong selection made")
main_menu()#intialise the program
